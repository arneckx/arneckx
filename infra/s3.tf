locals {
  file = "./resources/resume.pdf"
  favicon = "./resources/favicon.ico"
}

resource "aws_s3_bucket" "arneckx" {
  bucket = "arneckx"
}

resource "aws_s3_bucket_acl" "arneckx_acl" {
  bucket = aws_s3_bucket.arneckx.id
  acl    = "private"
}

resource "aws_s3_object" "curriculum_vitae" {
  bucket       = aws_s3_bucket.arneckx.bucket
  key          = "resume"
  source       = local.file
  content_type = "application/pdf"

  acl = "public-read"

  etag = filemd5(local.file)
}

resource "aws_s3_object" "favicon" {
  bucket       = aws_s3_bucket.arneckx.bucket
  key          = "favicon.ico"
  source       = local.favicon

  acl = "public-read"
  etag = filemd5(local.favicon)
}