provider "aws" {

  region = "eu-west-1"

  default_tags {
    tags = {
      Owner       = "arneckx"
      Application = "arneckx"
      Project     = "arneckx"
      Terraform   = "https://gitlab.com/arneckx/arneckx/infra"
    }
  }
}
