resource "vercel_project" "arneckx" {
  name      = "home"
  framework = "nextjs"
  git_repository = {
    type = "gitlab"
    repo = "arneckx/arneckx"
  }
}

resource "vercel_project_domain" "www_arneckx" {
  project_id = vercel_project.arneckx.id
  domain     = "www.arneckx.be"
}

resource "vercel_project_domain" "arneckx" {
  project_id = vercel_project.arneckx.id
  domain     = "arneckx.be"

  redirect             = vercel_project_domain.www_arneckx.domain
  redirect_status_code = 308
}