terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.14"
    }
    vercel = {
      source = "vercel/vercel"
      version = "~> 0.3"
    }
  }
}
