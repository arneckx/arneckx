## Arneckx

Hi my name is Arne, I am a freelance developer based in Belgium,
with a main focus on Java backend development.

Besides Java I try to work on personal development and learning new things. Currently, I am discovering the cloud world and everything that is part of it. Also clean code and software design is something I want to focus on.

If you catch me outside of work: I like to travel, I love cycling and coffee keeps me going!

Skills:
Java, Terraform, AWS, Docker, Kubernetes, Quarkus, Spring, Mongo DB, Scrum, Kotlin, GO, JIRA, GIT, Linux, Gradle, Maven, Elasticsearch, SQL, Android, HTML, CSS, PHP, Next JS, Gatsby JS, Jamstack, Tailwind, Javascript, React, Adobe Creative Cloud, CodeSoftware craftsmanship, Unix, ActiveMQ, Localstack, ...