import Head from 'next/head'
import {
  getAboutMe,
  getAllPortfolioItems,
  getConfigItem,
  getSkills,
  getWork,
} from '../lib/api'
import Hero from '../components/sections/Hero'
import AboutMe from '../components/sections/AboutMe'
import Contact from '../components/sections/Contact'
import Footer from '../components/sections/Footer'
import WorkExperience from '../components/sections/WorkExperience'
import Skills from '../components/sections/Skills'
import BackToTop from '../components/molecules/BackToTop'

export default function Home({ config, aboutMe, skills, work }) {
  return (
    <div>
      <Head>
        <title>{config.title}</title>
        <meta property="og:type" content="video.movie" />

        <meta name="description" content={config.description} />
        <meta
          name="keywords"
          content="Freelance,Java,Developer,Arne Hendrickx"
        />
        <meta property="og:type" content="website" />
        <meta property="og:title" content={config.title} />
        <meta property="og:description" content={config.description} />
        <meta property="og:site_name" content={config.title} />
        <meta property="og:image" content={config.image} />
        <meta property="og:url" content={config.url} />
        <meta charSet="utf-8"></meta>
        <link rel="icon" href={config.icon} />
      </Head>

      <main id={'top'}>
        <Hero />
        <div id={'content'}></div>
        <div className={'px-4 md:px-0'}>
          <div className={'grid content-center md:h-screen'}>
            <div
              className="mx-auto mt-16 grid max-w-2xl auto-cols-max grid-cols-1
                        space-y-4 px-4 md:px-0 lg:max-w-5xl lg:grid-cols-3"
            >
              <div className="lg:col-span-2 lg:mr-24">
                <AboutMe aboutMe={aboutMe} />
              </div>
              <WorkExperience work={work} />
              <div className="lg:col-span-2 lg:mr-24">
                <Skills skills={skills} />
              </div>
            </div>
          </div>
          <Contact />
          <BackToTop />
        </div>
        <Footer />
      </main>
    </div>
  )
}

export async function getStaticProps() {
  const allItems = getAllPortfolioItems([
    'slug',
    'content',
    'title',
    'type',
    'image',
    'date',
    'url',
    'skills',
  ])
  const config = getConfigItem()
  const aboutMe = getAboutMe()
  const skills = getSkills()
  const work = getWork()

  return {
    props: { allItems, config, aboutMe, skills, work },
  }
}
