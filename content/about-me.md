---
---

Besides Java I try to work on personal development and learning new things.
Currently, I am discovering the cloud world and everything that is part of it.
Also clean code and software design is something I want to focus on.\
\
If you catch me outside of work: I like to travel, I love cycling and coffee keeps me going!