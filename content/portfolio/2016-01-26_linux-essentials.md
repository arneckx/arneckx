---
title: 'LE-1: Linux Essentials'
type: 'certification'
image: '/assets/linux.jpg'
date: '2018-01-26T05:35:07.322Z'
skills: 'Linux, Unix'
---

Still at school learning the essentials of the Linux and open source industry. 
Certification where the major components of the Linux operating system are introduced.
