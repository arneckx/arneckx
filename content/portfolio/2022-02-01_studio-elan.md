---
title: 'Studio Elan'
type: 'website'
image: '/assets/social.jpg'
date: '2022-02-01T05:35:07.322Z'
skills: 'Next js, Javascript'
url: 'https://www.studio-elan.be'
---

A gift shop website where you can buy creative boxes. This website is build with the Stackbit tool and is developed using Next js.
