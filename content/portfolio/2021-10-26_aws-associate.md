---
title: 'AWS Certified Developer Associate'
type: 'certification'
image: '/assets/aws.jpg'
date: '2021-10-26T05:35:07.322Z'
skills: 'Amazon Web Services, cloud'
---

Making progress, to discover the cloud world and everything that is part of it.
