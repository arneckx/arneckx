---
title: 'HashiCorp Certified Terraform Associate'
type: 'certification'
image: '/assets/terraform.jpg'
date: '2021-09-14T05:35:07.322Z'
skills: 'Terraform, cloud, IaC'
---

After using Terraform for one year on project, I can say I know the foundations.
