---
title: 'Socially Ingenious'
type: 'certification'
image: '/assets/social.jpg'
date: '2018-06-30T05:35:07.322Z'
skills: 'social, soft skills'
---

With the socially ingenious program we aim to get closer to a community caring environment...
