---
title: 'Elcreador art gallery'
type: 'website'
image: '/assets/elcreador.png'
date: '2019-11-04T05:35:07.322Z'
skills: 'Gatsby, Contentful, Javascript'
url: 'https://www.elcreador.be/paintings/'
---

For my father I build a website showing off his painting. I worked with Gatbsy as framework to build it.
