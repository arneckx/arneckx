---
title: 'Certified Kubernetes Application Developer (CKAD)'
type: 'certification'
image: '/assets/kubernetes.jpg'
date: '2020-10-04T05:35:07.322Z'
skills: 'Kubernetes, cloud'
---

An interesting talk of Niels Claeys triggered me to pursuit the Kubernetes certificate.
