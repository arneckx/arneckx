module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: 'class',
  theme: {
    extend: {
      keyframes: {
        grow: {
          '0%': { transform: 'scale(1)' },
          '50%': { transform: 'scale(1.4)' },
          '100%': { transform: 'scale(1.5))' },
        },
        'fade-in-down': {
          '0%': {
            opacity: '0',
            transform: 'translateY(-10px)',
          },
          '100%': {
            opacity: '1',
            transform: 'translateY(0)',
          },
        },
        pulse: {
          '0%, 100%': {
            opacity: '1',
          },
          '50%': {
            opacity: '0',
          },
        },
        show: {
          '0%': {
            opacity: '0',
          },
          '100%': {
            opacity: '1',
          },
        },
      },
      animation: {
        'fade-in-down': 'fade-in-down 0.8s ease-out',
        cursor: 'pulse 1.5s cubic-bezier(.72,0,.31,1) 5',
        show: 'show 1s ease-out',
        grow: 'grow 1s',
      },
      colors: {
        houseBlue: {
          100: 'rgba(68,173,225,0.32)',
          200: '#44ADE1',
          400: 'rgba(0,79,118,0.79)',
          500: '#004F76',
          900: '#1C465C',
        },
        cv1: '#4B8C85',
        cv2: '#CF6250',
        cv3: '#394C5B',
        cv4: '#E5FB53',
      },
    },
    fontFamily: {
      sans: ['Open Sans', 'ui-sans-serif', 'system-ui'],
      serif: ['ui-serif', 'Georgia'],
      mono: ['ui-monospace', 'SFMono-Regular'],
      display: ['Shadows Into Light'],
      body: ['Open Sans', 'ui-sans-serif'],
    },
  },
}
