export default function AboutMe({ aboutMe, skills }) {
  return (
    <section>
      <h1 className={'font-large font-display text-7xl'}>About Me</h1>
      <div className={'text-base md:text-xl'}>
        <div dangerouslySetInnerHTML={aboutMe.content} />
      </div>
    </section>
  )
}
