import ReferenceLink from '../atoms/ReferenceLink'

export default function Contact() {
  return (
    <section id={'contact-section'} className={'pt-32'}>
      <div
        className={
          'mx-auto mb-32 max-w-2xl px-4 font-display md:px-0 lg:max-w-5xl'
        }
      >
        <div className={'flex flex-row items-center text-2xl lg:text-4xl'}>
          Add me on{' '}
          <ReferenceLink
            href={'https://www.linkedin.com/in/arne-hendrickx/'}
            title={'Linkedin'}
          />
        </div>
        <div className={'flex flex-row items-center text-2xl lg:text-4xl'}>
          See some code on{' '}
          <ReferenceLink href={'https://gitlab.com/arneckx'} title={'Gitlab'} />
        </div>
        <div className={'flex flex-row items-center text-2xl lg:text-4xl'}>
          Send me an{' '}
          <ReferenceLink
            href={'mailto:info@arneckx.be'}
            title={'E-mail'}
            copyToClipboardText={'info@arneckx.be'}
          />
        </div>
        <div className={'flex flex-row items-center text-2xl lg:text-4xl'}>
          Give me a{' '}
          <ReferenceLink
            href={'tel:+32494864805'}
            title={'Call'}
            copyToClipboardText={'+32494864805'}
          />
        </div>
      </div>
    </section>
  )
}
