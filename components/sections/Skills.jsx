export default function Skills({ skills }) {
  return (
    <section>
      <div className={'space-y-16 text-base md:text-xl'}>
        <div>
          <p className={'font-bold'}>Skills:</p>
          {skills.map((skill) => (
            <span
              key={skill.name}
              className={`mx-1 inline-flex items-center rounded-md px-2 py-1 text-xs font-medium ${
                skill.level === 'xxx'
                  ? 'bg-cv4 font-bold text-gray-900'
                  : 'bg-gray-100 text-gray-600'
              }`}
            >
              {skill.name}
            </span>
          ))}
        </div>
      </div>
    </section>
  )
}
