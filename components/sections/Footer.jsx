import Image from 'next/image'
import Company from './Company'

export default function Footer() {
  return (
    <section className={'mx-4 mt-32 pb-4 md:mx-16'}>
      <hr className="mx-auto my-4 w-2/3 rounded border-0 bg-zinc-200 h-[1.5px] dark:bg-gray-700 md:my-10" />
      <div className={'grid md:grid-cols-2'}>
        <div>
          <Company />
        </div>
        <div
          className={'flex place-items-end gap-8 place-self-end text-zinc-700'}
        >
          <p className={'text-sm'}>&copy; {new Date().getFullYear()} Arneckx. All rights reserved.</p>
          <div style={{ width: '4rem' }}>
            <Image
              priority={true}
              src={'/logo-arneckx-mark.png'}
              width={209}
              height={228.5}
              alt={'logo arneckx'}
              sizes="100vw"
              style={{
                width: '100%',
                height: 'auto',
              }}
            />
          </div>
        </div>
      </div>
    </section>
  )
}
