export default function Company() {
  let company = {
    name: 'arneckx BV',
    number: 'BE 0784.376.840',
    mail: 'info@arneckx.be',
    phoneNumber: '+32 494 86 48 05',
    address: 'Zomerstraat 26, 9270 Kalken (Belgium)',
    iban: 'BE61 7350 6024 9617',
  }

  return (
    <div>
      <div className="px-4 sm:px-0">
        <h3 className="text-base font-semibold leading-7 text-gray-900">
          {company.name}
        </h3>
        <p className="max-w-2xl text-sm leading-6 text-gray-500">
          Company Information
        </p>
      </div>
      <div className="mt-6 text-sm">
        <dl className="grid grid-cols-1 sm:grid-cols-2">
          <div className="border-t border-gray-100 px-4 py-2 sm:col-span-2 sm:px-0">
            <dt className="font-medium leading-6 text-gray-900">Address</dt>
            <dd className="text-gray-700 sm:mt-2">{company.address}</dd>
          </div>
          <div className="border-t border-gray-100 px-4 py-2 sm:col-span-1 sm:px-0">
            <dt className="font-medium text-gray-900">Number</dt>
            <dd className="text-gray-700 sm:mt-2">{company.number}</dd>
          </div>
          <div className="border-t border-gray-100 px-4 py-2 sm:col-span-1 sm:px-0">
            <dt className="font-medium text-gray-900">IBAN</dt>
            <dd className="text-gray-700 sm:mt-2">{company.iban}</dd>
          </div>
          <div className="border-t border-gray-100 px-4 py-2 sm:col-span-1 sm:px-0">
            <dt className="font-medium text-gray-900">Mail</dt>
            <dd className="text-gray-700 sm:mt-2">{company.mail}</dd>
          </div>
          <div className="border-t border-gray-100 px-4 py-2 sm:col-span-1 sm:px-0">
            <dt className="font-medium text-gray-900">Phone</dt>
            <dd className="text-gray-700 sm:mt-2">{company.phoneNumber}</dd>
          </div>
        </dl>
      </div>
    </div>
  )
}
