import Image from 'next/image'
import { Link } from 'react-scroll'
import clsx from 'clsx'
import { ArrowDownIcon } from '@heroicons/react/24/outline'

function Photos() {
  let rotations = ['rotate-2', '-rotate-2', 'rotate-2', 'rotate-2', '-rotate-2']

  return (
    <div className="mt-16 animate-show sm:mt-20">
      <div className="-my-4 flex justify-center gap-5 overflow-hidden py-4 sm:gap-8">
        {[
          '/arneckx-1.jpg',
          '/arneckx-2.jpg',
          '/arneckx-3.jpg',
          '/arneckx-4.jpg',
          '/arneckx-5.jpg',
        ].map((image, imageIndex) => (
          <div
            key={image}
            className={clsx(
              'relative aspect-[9/10] w-44 flex-none overflow-hidden rounded-xl sm:w-72 sm:rounded-2xl',
              rotations[imageIndex % rotations.length],
            )}
          >
            <Image
              src={image}
              alt=""
              width={209}
              height={462}
              sizes="(min-width: 640px) 18rem, 11rem"
              className="absolute inset-0 h-full w-full object-cover"
            />
          </div>
        ))}
      </div>
    </div>
  )
}

export default function Hero() {
  return (
    <section className={'bg-houseBlue-500 pb-24 md:h-screen md:pb-0'}>
      <div className={'mx-auto max-w-2xl px-4 md:px-0 lg:max-w-5xl'}>
        <div className={'animate-fade-in-down pt-16 md:pt-8'}>
          <div style={{ width: '16rem' }}>
            <Image
              priority={true}
              src={'/logo-white.png'}
              width={1687}
              height={462}
              alt={'logo arneckx'}
              sizes="100vw"
              style={{
                width: '100%',
                height: 'auto',
              }}
            />
          </div>
        </div>

        <div className={'mt-16 animate-show md:mt-32'}>
          <h2 className={'font-display text-4xl text-gray-200 md:text-7xl'}>
            Software developer{' '}
            <span className={'text-5xl md:text-6xl'}>👋</span>
          </h2>
          <div className={'mt-6 max-w-2xl text-base text-gray-200 md:text-xl'}>
            <p>
              Hi my name is Arne, I am a freelance developer based in Belgium,
              <br /> with a main focus on Java backend development.
            </p>
            <br />
            <p>
              Currently passioned about Java ☕️ Cloud ☁️ Devops 👨‍💻 Webdesign 🖥️
            </p>
          </div>
          <div
            className={
              'justify-left mt-11 flex cursor-pointer text-gray-100 md:mt-10 lg:mt-8'
            }
          >
            <Link spy={true} to="content" smooth={true} duration={500}>
              <div className={'flex flex-col justify-center '}>
                <p className={'w-full font-display text-xl'}>See more</p>
                <ArrowDownIcon className="mt-4 h-10 w-full animate-bounce" />
              </div>
            </Link>
          </div>
        </div>
      </div>

      <Photos />
    </section>
  )
}
