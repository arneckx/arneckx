import {Link} from 'react-scroll'
import {ArrowUpIcon} from '@heroicons/react/24/outline'

export default function BackToTop() {
  return (
    <div
      className={
        'mb-32 mt-32 flex cursor-pointer justify-center text-houseBlue-500 md:mb-0 md:mt-8'
      }
    >
      <Link spy={true} to="top" smooth={true} duration={500}>
        <div className={'flex flex-col justify-center '}>
          <ArrowUpIcon className="h-10 w-full" />
          <p className={'w-full font-display text-xl'}>To the top</p>
        </div>
      </Link>
    </div>
  )
}
