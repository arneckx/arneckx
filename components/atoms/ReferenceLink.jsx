export default function ReferenceLink({ href, title, copyToClipboardText }) {
  if (copyToClipboardText) {
    return (
      <div
        id={title}
        className="mx-8 flex flex-row flex-wrap items-center justify-center text-sm text-gray-700 font-body sm:mx-16 md:mx-0"
      >
        <div className={'md:hidden'}>
          <a href={href}>{referenceObject()}</a>
        </div>

        <div
          id={'trigger-tooltip-copied-' + title}
          className={'hidden cursor-pointer md:block'}
          onClick={(event) => copy(copyToClipboardText)}
        >
          {referenceObject()}
        </div>
        <div
          id={'tooltip-copied-' + title}
          role="tooltip"
          className="invisible absolute z-10 inline-block rounded-sm bg-gray-200 p-4 text-sm font-medium text-gray-800 opacity-0 shadow-sm tooltip dark:bg-gray-700 dark:text-gray-200"
        >
          Copied {copyToClipboardText} to clipboard
          <div className="tooltip-arrow" data-popper-arrow="top" />
        </div>
      </div>
    )
  } else {
    return <a href={href}>{referenceObject()}</a>
  }

  function copy(data) {
    navigator.clipboard.writeText(data)

    const targetEl = document.getElementById('tooltip-copied-' + title)
    const triggerEl = document.getElementById('trigger-tooltip-copied-' + title)

    const options = {
      placement: 'right',
      triggerType: 'click',
      onShow: () => {
        setTimeout(() => {
          tooltip.hide()
        }, 2000)
      },
    }

    const tooltip = new Tooltip(targetEl, triggerEl, options)

    tooltip.show()
  }

  function referenceObject() {
    return (
      <div
        className={
          'duration-250 mx-8 flex h-16 w-16 rounded-full bg-houseBlue-500 font-body transition delay-100 ease-out hover:scale-150 lg:h-20 lg:h-20 lg:w-20 lg:w-20'
        }
      >
        <div className={'m-auto text-base text-gray-100 md:text-lg'}>
          {title}
        </div>
      </div>
    )
  }
}
