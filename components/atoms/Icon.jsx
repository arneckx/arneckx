import {
  BuildingLibraryIcon,
  ComputerDesktopIcon,
  NewspaperIcon,
  TruckIcon,
} from '@heroicons/react/24/outline'

function heroIconFor(name) {
  switch (name) {
    case 'DesktopComputerIcon':
      return <ComputerDesktopIcon className="h-7 w-7 p-1" />
    case 'NewspaperIcon':
      return <NewspaperIcon className="h-7 w-7 p-1" />
    case 'LibraryIcon':
      return <BuildingLibraryIcon className="h-7 w-7 p-1" />
    case 'TruckIcon':
      return <TruckIcon className="h-7 w-7 p-1" />
    default:
      return <ComputerDesktopIcon className="h-7 w-7 p-1" />
  }
}

export default function Icon({ iconName }) {
  return heroIconFor(iconName)
}
