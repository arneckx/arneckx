export default function PortfolioItemType({ type, toggleType, isActive }) {
  return (
    <div
      className={`${
        isActive ? 'bg-black text-white' : 'bg-white text-gray-700'
      } 
            leading-sm hover:bg-gray ml-4 rounded-sm border bg-black px-8 py-1 py-2 text-sm font-bold uppercase text-white shadow-sm`}
      onClick={() => toggleType(type)}
    >
      <div>
        <h2>{type}</h2>
      </div>
    </div>
  )
}
