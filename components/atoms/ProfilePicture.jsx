import Image from 'next/image'

export default function ProfilePicture({ alt, src }) {
  return (
    <div className="flex justify-center transition duration-700">
      <div className={'animate-fade-in-down'}>
        <Image
          priority={false}
          src={src}
          width={200}
          height={200}
          className="rounded-sm"
          alt={alt}
          style={{
            maxWidth: '100%',
            height: 'auto',
          }}
        />
      </div>
    </div>
  )
}
