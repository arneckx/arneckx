import fs from 'fs'
import {join} from 'path'
import matter from 'gray-matter'
import {ABOUT_ME_LOCATION, CONFIG_LOCATION, POSTS_DIRECTORY, SKILLS_LOCATION, WORK_LOCATION} from "./constants";
import {marked} from "marked";

const postsDirectory = join(process.cwd(), POSTS_DIRECTORY)
const config = join(process.cwd(), CONFIG_LOCATION)
const aboutMe = join(process.cwd(), ABOUT_ME_LOCATION)
const skills = join(process.cwd(), SKILLS_LOCATION)
const work = join(process.cwd(), WORK_LOCATION)

function getPortfolio() {
    return fs.readdirSync(postsDirectory);
}

function getPortfolioItem(fileName, fields = []) {
    const slug = fileName.replace(/\.md$/, '')
    const fullPath = join(postsDirectory, `${slug}.md`)
    const {data, content} = matter(fs.readFileSync(fullPath, 'utf8'))

    const item = {}

    fields.forEach((field) => {
        if (field === 'slug') {
            item[field] = slug
        }
        if (field === 'content') {
            item[field] = content
        }
        if (fieldIsPresent(data, field)) {
            item[field] = data[field]
        }
    })

    return item
}

function fieldIsPresent(data, field) {
    return typeof data[field] !== 'undefined';
}

export function getAllPortfolioItems(fields = []) {
    return getPortfolio()
        .map((fileName) => getPortfolioItem(fileName, fields))
        .sort((post1, post2) => (post1.date > post2.date ? -1 : 1))
}

export function getConfigItem() {
    const {data} = matter(fs.readFileSync(config, 'utf8'))
    return data;
}

export function getAboutMe() {
    const {content} = matter(fs.readFileSync(aboutMe, 'utf8'))
    return {
        content: {__html: marked(content)}
    };
}

export function getSkills() {
    return JSON.parse(fs.readFileSync(skills, 'utf8'));
}

export function getWork() {
    return JSON.parse(fs.readFileSync(work, 'utf8'));
}